﻿using Pvz1.DataObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pvz1
{
    class ObjectParser
    {


        public List<Temperature> parseTemperature(string filename) {
            List<Temperature> temp = File.ReadLines(filename).Select(line => new Temperature(line)).ToList();

            return temp.FindAll(Temperature.getYearPredicate);
        }

        public List<CountryPoints> parseCountryPoints(string xFile, string yFile) {
            List<double> xValues = File.ReadAllLines(xFile)[0].Split(',').Select(item => Double.Parse(item)).ToList();
            List<double> yValues = File.ReadAllLines(yFile)[0].Split(',').Select(item => Double.Parse(item)).ToList();

            List<CountryPoints> countryPoints = new List<CountryPoints>();
            for (int i = 0; i < xValues.Count(); i++) {
                countryPoints.Add(new CountryPoints(xValues[i], yValues[i]));
            }

            return countryPoints;
        }
    }
}
