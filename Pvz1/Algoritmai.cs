﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Complex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace Pvz1
{
    class Algoritmai
    {

        public double xMin { get; set; }
        public double xMax { get; set; }
        public int N { get; set; }
        public int atvTaskai { get; set; }
        public Func<double, double> funkcija { get; set; }


        public Algoritmai() { }

        public Series atvaizduotiFunkcija(string aprasymas) 
        {
            Series series = new Series(aprasymas);
            series.ChartType = SeriesChartType.Line;
            series.BorderWidth = 2;
            for (double x = xMin; x < xMax; x += ((xMax - xMin) / atvTaskai))
            {
                series.Points.AddXY(x, funkcija(x));
            }
            return series;
        }

        public Series atvaizduotiFunkcija() {
                return this.atvaizduotiFunkcija("F(x)");
        }

        public Series atvaizduotiAbscises(List<double> abscises, string aprasymas) {
            Series series = new Series(aprasymas);
            series.MarkerSize = 7;
            series.ChartType = SeriesChartType.Point;

            foreach(double abscise in abscises) {
                series.Points.AddXY(abscise, funkcija(abscise));
            }

            return series;
        }

        public Series atvaizduotiInterpoliavima(List<double> abscises, string aprasymas) {
            Series series = new Series(aprasymas);
            series.ChartType = SeriesChartType.Line;
            series.BorderWidth = 2;

            for (double x = xMin; x < xMax; x += ((xMax - xMin) / atvTaskai))
            {
                double F = 0;
                for (int i = 0; i < abscises.Count(); i++) {
                    F = F + (lagrandge(abscises, i, x) * funkcija(abscises[i]));
                }
                series.Points.AddXY(x, F);
            }

            return series;
        }

        public Series atvaizuotiSplaina(List<double> abscises, List<double> koeficientai, string aprasymas) {
            Series series = new Series(aprasymas);
            series.ChartType = SeriesChartType.Point;
            series.BorderWidth = 2;

            for (int i = 0; i < N - 1; i++) {
 
                    for (double x = abscises[i]; x < abscises[i + 1]; x += ((abscises[i + 1] - abscises[i]) / atvTaskai))
                    {
                        double d = abscises[i + 1] - abscises[i];
                        double F = ((koeficientai[i] / 2) * Math.Pow(x - abscises[i], 2)) + (((koeficientai[i + 1] - koeficientai[i]) / (6.0 * d)) * Math.Pow(x - abscises[i], 3)) + ((x - abscises[i]) * (((funkcija(abscises[i + 1]) - funkcija(abscises[i])) / d) - (koeficientai[i] * (d / 3.0)) - (koeficientai[i + 1] * (d / 6.0)))) + funkcija(abscises[i]);
                        series.Points.AddXY(x, F);
                    }
            }

            return series;
        }

        public Series atvaizduotiNetikti(List<double> abscises, string aprasymas) {
            Series series = new Series(aprasymas);
            series.ChartType = SeriesChartType.Line;

            for (double x = xMin; x < xMax; x += ((xMax - xMin) / atvTaskai))
            {
                double F = 0;
                for (int i = 0; i < abscises.Count(); i++)
                {
                    F = F + (lagrandge(abscises, i, x) * funkcija(abscises[i]));
                }

                series.Points.AddXY(x, funkcija(x) - F);
            }

            return series;
        }

        public List<double> ciobysevoAbscises() {
            List<double> abscises = new List<double>();

            for (int k = 0; k < N; k++) { 
                double abscise = ((xMax + xMin) / 2) + ((xMax - xMin) / 2 * Math.Cos((Math.PI * ((2 * k) + 1)) / (2 * N)));
                abscises.Add(abscise);
            }

            return abscises;
        }

        public List<double> tolygiosAbscises() {
            List<double> abscises = new List<double>();
            double intervalas = (xMax - xMin) / (N - 1);
            for (double i = xMin; i <= xMax; i += intervalas) {
                abscises.Add(i);
            }
            return abscises;
        }

        public double lagrandge(List<double> abscises, int j, double x) {
            double L = 1.0;
 
            for (int i = 0; i < abscises.Count ; i++) {
                if (i != j) {
                    L = L * (x - abscises[i]) / (abscises[j] - abscises[i]);
                }
            }

            return L;
        }


        public List<double> splineCoef(List<double> abscises)
        {
            int n = abscises.Count();

            Matrix<double> A = Matrix<double>.Build.Dense(n, n, 0.0);
            Matrix<double> B = Matrix<double>.Build.Dense(n, 1, 0.0);
            List<double> d = new List<double>();

            for (int i = 1; i < n; i++) {
                d.Add(abscises[i] -abscises[i - 1]);
            }

            for (int i = 0; i < n - 2; i++) {
                A[i, i] = (double) (d[i] / 6.0);
                A[i, i + 1] = (double) ((d[i] + d[i + 1]) / 3.0);
                A[i, i + 2] = (double) (d[i + 1] / 6.0);
                B[i,0] = ((funkcija(i + 2) - funkcija(i + 1)) / d[i + 1]) - ((funkcija(i + 1) - funkcija(i)) / d[i]);
            }

            A[n - 1, 0] = 1;
            A[n - 1, n - 1] = 1;
            
            Matrix<double> X = (A.Transpose()*A).Inverse()*A.Transpose() * B;

            List<double> coefs = new List<double>();
            for (int i = 0; i < n; i++)
            {
                coefs.Add(X[i, 0]);
            }

            return coefs;
        }

        public List<double> itemptoSplainoKoeficientai(List<double> X, List<double> Y, List<double> sgm) {
            int n = X.Count();
            Matrix<double> A = Matrix<double>.Build.Dense(n, n, 0.0);
            Matrix<double> B = Matrix<double>.Build.Dense(n, 1, 0.0);
            List<double> d = new List<double>();
            for(int i = 1; i < n; i++)
            {
                d.Add(X[i] - X[i - 1]);
            }

            for (int i = 0; i < n - 2; i++) {
                double sg1 = sgm[i];
                double sg2 = sgm[i + 1];
                A[i,i] = (1.0 / (Math.Pow(sg1, 2) * d[i])) - (1.0 / (sg1 * Math.Sinh(sg1 * d[i])));
                A[i, i + 1] = (Math.Cosh(sg1 * d[i]) / (sg1 * Math.Sinh(sg1 * d[i]))) + (Math.Cosh(sg2 * d[i + 1]) / (sg2 * Math.Sinh(sg2 * d[i + 1]))) - (1.0 / (Math.Pow(sg1, 2) * d[i])) - (1.0 / (Math.Pow(sg2, 2) * d[i + 1]));
                A[i, i + 2] = (1.0 / (Math.Pow(sg2, 2) * d[i+1])) - (1.0 / (sg2 * Math.Sinh(sg2 * d[i + 1])));
                B[i, 0] = ((Y[i + 2] - Y[i + 1]) / (d[i + 1])) - ((Y[i + 1] - Y[i]) / d[i]);
            }

            A[n - 2, 0] = 1;
            A[n - 1, n - 1] = 1;
            B[n - 2, 0] = 0;
            B[n - 1, 0] = 0;

            Matrix<double> DDF = (A.Transpose() * A).Inverse() * A.Transpose() * B;
            List<double> coefs = new List<double>();
            for (int i = 0; i < n; i++)
            {
                coefs.Add(DDF[i, 0]);
            }
            coefs[0] = 0;
            coefs[coefs.Count() - 1] = 0;
            return coefs;
        }

        public List<double> gautiT(List<double> X, List<double> Y) {
            List<double> koef = new List<double>();
            koef.Add(0);


            for (int i = 1; i < X.Count(); i++) {
                Matrix<double> mat1 = Matrix<double>.Build.Dense(1, 2, 0.0);
                Matrix<double> mat2 = Matrix<double>.Build.Dense(1, 2, 0.0);
                mat1[0, 0] = X[i]; mat1[0, 1] = Y[i];
                mat2[0, 0] = X[i - 1]; mat2[0, 1] = Y[i - 1];
                koef.Add(koef[i - 1] + (mat1 - mat2).L2Norm());
            }

            return koef;
        }

        public Series itemptuSplainuInterpoliavimas(List<double> X, List<double> Y, List<double> t, List<double> sg, string description)
        {
            Series series = new Series(description);
            series.ChartType = SeriesChartType.Line;
            series.BorderWidth = 2;

            int n = X.Count();

            List<double> DDFX = this.itemptoSplainoKoeficientai(t, X, sg);
            List<double> DDFY = this.itemptoSplainoKoeficientai(t, Y, sg);

            for (int i = 0; i < N - 1; i++)
            {
                double xInter = (t[i + 1] - t[i]) / (atvTaskai - 1);
                List<double> xRange = getRange(t[i], t[i + 1], xInter);
                foreach (double x in xRange) {
                    double SX = itemptasSplainas(t, X, DDFX, sg[i], i, x);
                    double SY = itemptasSplainas(t, Y, DDFY, sg[i], i, x);
                    series.Points.AddXY(SX, SY);
                }
            }

            return series;
        }

        private List<double> getRange(double x1, double x2, double inter) {
            List<double> range = new List<double>();
            if (x1 <= x2)
            {
                for (double x = x1; x < x2; x += inter) {
                    range.Add(x);
                }

            }
            else {
                for (double x = x1; x >= x2; x += inter)
                {
                    range.Add(x);
                }
            }
            return range;
        }

        private double itemptasSplainas(List<double> X, List<double> Y, List<double> DDF, double sgm, int i, double x) {
            double d = X[i + 1] - X[i];
            return (DDF[i] / (Math.Pow(sgm, 2))) * (Math.Sinh(sgm * (d - (x - X[i]))) / Math.Sinh(sgm * d)) +
                ((Y[i] - (DDF[i] / Math.Pow(sgm, 2))) * ((d - (x - X[i])) / d)) +
                (DDF[i + 1] / Math.Pow(sgm, 2)) * (Math.Sinh(sgm * (x - X[i])) / Math.Sinh(sgm * d)) +
                ((Y[i + 1] - (DDF[i + 1] / Math.Pow(sgm, 2))) * ((x - X[i]) / d));
        }

        public List<double> gautiItempimoKoeficientus(int n, double reiksme) {
            List<double> koef = new List<double>();
            for (int i = 0; i < n; i++) {
                koef.Add(reiksme);
            }

            return koef;
        }

        public List<double> skaiciuotiFunkc(List<double> parametrai) {
            List<double> yReiksm = new List<double>();

            foreach(double x in parametrai) {
                yReiksm.Add(funkcija(x));
            }

            return yReiksm;
        } 
    }
}
