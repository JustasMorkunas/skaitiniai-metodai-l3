﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pvz1.DataObjects
{
    interface DataObject
    {
        void parse(string[] values);
    }
}
