﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pvz1.DataObjects
{
    class Temperature : DataObject
    {
        double temperature { get; set; }
        string date { get; set; }
        int month { get; set; }

        public Temperature(string line) {
            parse(line.Split(','));
        }

        public void parse(string[] values)
        {
            temperature = Double.Parse(values[0].Trim());
            date = values[1].Trim();
            string monthStr = values[2].Replace("Average", "").Trim();

            string[] months = CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedMonthNames;
            month = Array.IndexOf(months, monthStr);
        }

        public static bool getYearPredicate(Temperature temp) {
            return temp.date.Equals(Settings.FILTER_YEAR);
        }

        public int getMonth() {
            return month;
        }

        public double getTemperature() {
            return temperature;
        }
    }
}
