﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pvz1.DataObjects
{
    class CountryPoints
    {
        double X { get; set; }
        double Y { get; set; }

        public CountryPoints(double x, double y) {
            X = x;
            Y = y;
        }

        public double getX() {
            return X;
        }

        public double getY() {
            return Y;
        }
    }
}
